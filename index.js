const fs = require("fs")
const Discord = require('discord.js')

const DATA_DIRECTORY = process.env.DATA_DIRECTORY
const DISCORD_TOKEN = process.env.DISCORD_TOKEN

const STORE_FILE = 'store.json'

let store
try {
    store = require(`${DATA_DIRECTORY}/${STORE_FILE}`)
} catch (err) {
    store = {}
}

const client = new Discord.Client()

function createGuildStoreIfNotExists(guildID) {
    if (typeof store[guildID] === 'undefined') {
        store[guildID] = {channels: [], users: {}}
    }
}

function addChannel (guildID, data) {
    createGuildStoreIfNotExists(guildID)

    if (!store[guildID].channels.includes(guildID)) {
        store[guildID].channels.push(guildID)
    }
}

function setUser(user, {name, race, job}) {
    createGuildStoreIfNotExists(guildID)

    const guildStore = store[guildID]
    if (!hasUser(guildID, user.id)) {
        guildStore.users[userID] = {id: userID, normalName: user.nickname}
    }

    const storeUser = guildStore.users[userID]
    Object.assign(storeUser, {name: name || storeUser.name, race: race || storeUser.race, job: job || storeUser.job})
}

function hasUser(guildID, userID) {
    return Object.keys(store[guildID].users).includes(userID)
}

function changeNicknameToNormal (guildID, user) {
    if (!hasUser(guildID, user.id)) {
        return
    }

    user.setNickname(store[guildID].users[user.id].normalName)
}

function changeNicknameToTemp (user) {
    if (!hasUser(guildID, user.id)) {
        return
    }

    const storeUser = store[guildID].users[user.id]
    const nickname = storeUser.name
    user.setNickname(nickname)
}

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}`)
})

client.on('message', msg => {
    if (msg.author.id === client.user.id) {
        // ignore self
        return
    }

    if (msg.mentions.length === 0) {
        return
    }
    if (msg.mentions.keyArray().includes(client.user.id)) {
        console.log(msg.content)
        console.log(msg.cleanContent)

        return
        const what = ''
        const data = ''
        const guild = msg.guild.id
        // TODO: insertions
        switch (what) {
            case 'nom':
            case 'name':
            case 'race':
            case 'classe':
            case 'class':
            case 'add_channel':
                addChannel(guild, data)
        }
    }
})

client.on('voiceStateUpdate', (oldState, newState) => {
    if (CHANNELS_ID.includes(oldState.voiceChannelID) && CHANNELS_ID.includes(newState.voiceChannelID)) {
        return
    }
    if (!CHANNELS_ID.includes(oldState.voiceChannelID) && !CHANNELS_ID.includes(newState.voiceChannelID)) {
        return
    }
    
    if (!CHANNELS_ID.includes(oldState.voiceChannelID) && CHANNELS_ID.includes(newState.voiceChannelID)) {
        changeNicknameToTemp(newState)
    } else if (CHANNELS_ID.includes(oldState.voiceChannelID) && !CHANNELS_ID.includes(newState.voiceChannelID)) {
        changeNicknameToNormal(newState)
    }
})

// Save store every 5 seconds
setInterval(5000, () => {
    fs.writeFile(`${DATA_DIRECTORY}/${STORE_FILE}`, JSON.stringify(store), "utf8", (err) => err ? console.error(err) : console.log('Store saved to disk'));
})

client.login(DISCORD_TOKEN)